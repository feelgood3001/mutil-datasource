package tfp.com.tfp.demo.service;

import tfp.com.tfp.demo.model.City;

public interface CityService {
    public City selectCityByName(String  cityName);

    public long addCity(City city);
}
