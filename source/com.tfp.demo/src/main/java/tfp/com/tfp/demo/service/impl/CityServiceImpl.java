package tfp.com.tfp.demo.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tfp.com.tfp.demo.dao.cluster.CityDaoCluster;
import tfp.com.tfp.demo.dao.master.CityDao;
import tfp.com.tfp.demo.model.City;
import tfp.com.tfp.demo.service.CityService;

@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private CityDao cityDao;
    @Autowired
    private CityDaoCluster cityDaoCluster;

    @Override
    public City selectCityByName(String  cityName) {
        //LogFactory.useLog4JLogging();
        Page p= PageHelper.startPage(1,2);
        City city=cityDaoCluster.findByName(cityName);
        System.out.println("pageNum:"+p.getPageNum()+",pageSize:"+p.getPageSize()+",total:"+p.getTotal());
        return city;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 3600,rollbackFor =Exception.class )
    public long addCity(City city) {
        City hebei=new City();
        hebei.setId(10L);
        hebei.setCityName("河北");
        hebei.setDescription("河北");
        hebei.setProvinceId(1L);

        City henan=new City();
        henan.setId(20L);
        henan.setCityName("河南");
        henan.setDescription("河南");
        henan.setProvinceId(2L);

        cityDao.add(hebei);
        cityDao.add(henan);

        return 0;
    }
}
