package tfp.com.tfp.demo.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tfp.com.tfp.demo.model.City;
import tfp.com.tfp.demo.service.CityService;

//import java.util.logging.Logger;

@RestController
public class DemoController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private CityService cityService;



    @RequestMapping("/hello")
    public City index(String cityName){
        logger.error("莫非");
        City city=cityService.selectCityByName(cityName);
        System.out.println("测试");
        return city;
    }

    @RequestMapping("/add")
    public long add(String cityName){
        long id=cityService.addCity(null);
        return id;
    }
}
